package db

import (
	"context"
	"fmt"
	"testing"
)

func TestAutoIncrement(t *testing.T) {
	db := GetConnection()
	defer db.Close()
	ctx := context.Background()

	email := "ben@gmail.com"
	comment := "woyy berantem yokk"
	sql := "INSERT INTO comment(email, comment) values (?, ?)"

	result, err := db.ExecContext(ctx, sql, email, comment)
	if err != nil {
		panic(err)
	}

	insertId, err := result.LastInsertId()
	if err != nil {
		panic(err)
	}
	fmt.Println("last insert id ", insertId)
}
