package db

import (
	"context"
	"fmt"
	"strconv"
	"testing"
)

func TestDbTransaction(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	// *memulai Transaction
	tx, err := db.Begin()

	if err != nil {
		panic(err)
	}

	sql := "INSERT INTO comment(email, comment) VALUES (?, ?) "
	for i := 0; i < 10; i++ {
		email := "benlur" + strconv.Itoa(i) + "@gmail.com"
		comment := "hayok gua ajakin temen ke " + strconv.Itoa(i) + " join"

		// *pada proses ExecContext gunakan transaction
		result, err := tx.ExecContext(ctx, sql, email, comment)
		if err != nil {
			panic(err)
		}

		inserted, err := result.LastInsertId()
		if err != nil {
			panic(err)
		}
		fmt.Println("masuk data ", inserted)
	}

	// !jika kemungkinan ada error manfaatkan Rollback(), untuk menggagalkan input data
	err = tx.Rollback()

	//	*last commit this transaction if data is done
	//err = tx.Commit()
	if err != nil {
		panic(err)
	}
}
