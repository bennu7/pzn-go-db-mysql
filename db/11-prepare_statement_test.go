package db

import (
	"context"
	"fmt"
	"strconv"
	"testing"
)

/*
* menggunakan statement dapat menginput data berkali" dengan bantuan db pooling
 */
func TestPrepareStatement(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	sql := "INSERT INTO comment(email, comment) VALUES  (?,?)"
	stmt, err := db.PrepareContext(ctx, sql)
	if err != nil {
		panic(err)
	}
	// * wajib close setelah menggunakan statement
	defer stmt.Close()

	for i := 0; i < 10; i++ {
		email := "ibnu" + strconv.Itoa(i) + "@gmail.com"
		comment := "sini anak ke " + strconv.Itoa(i) + " dateng ke ring gua"

		result, err := stmt.ExecContext(ctx, email, comment)
		if err != nil {
			fmt.Println("err stmt.ExecContext ")
			panic(err)
		}
		inserted, err := result.LastInsertId()
		fmt.Println("last inserted ", inserted)
	}
}
