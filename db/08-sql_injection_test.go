package db

import (
	"context"
	"fmt"
	"testing"
)

func TestSqlInjectionInsert(t *testing.T) {
	db := GetConnection()
	username := "bennuu; #"
	password := "12345"

	ctx := context.Background()
	// *gunakan ini, tanda tanya agar aman dari SQL injection
	sql := `INSERT INTO user(username, password) VALUES  (?, ?)`
	result, err := db.ExecContext(ctx, sql, username, password)
	if err != nil {
		panic(err)
	}
	fmt.Println("success ", &result)
}

func TestSqlInjection(t *testing.T) {
	db := GetConnection()
	username := "ben'; #"
	password := "12345"

	ctx := context.Background()
	// !jangan menggunakan ini, ini berpotensi SQL injection
	//sql := `SELECT username FROM user WHERE username "`+username+`" AND password = "`+password+"' LIMIT 1"
	// *gunakan ini, tanda tanya agar aman dari SQL injection
	sql := `SELECT username FROM user WHERE username = ? AND password = ? LIMIT 1` // *using parameter
	rows, err := db.QueryContext(ctx, sql, username, password)
	fmt.Println(username, password)
	if err != nil {
		panic(err)
	}

	if rows.Next() {
		var username string

		err := rows.Scan(&username)
		if err != nil {
			panic(err)
		}
		fmt.Println("success login : ", username)
	} else {
		fmt.Println("failed login")
	}
}
