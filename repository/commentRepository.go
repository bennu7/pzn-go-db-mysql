package repository

import (
	"context"
	"github.com/bennu7/pzn-go-db-mysql/entity"
)

// ?terlebih dahulu buat Model Comment, baru lanjut buat interface Repository untuk Comment,
// ?setelah itu lanjut implementasi tiap" method
type CommentRepository interface {
	Insert(ctx context.Context, comment entity.Comment) (entity.Comment, error)
	FindById(ctx context.Context, id int32) (entity.Comment, error)
	FindAll(ctx context.Context) ([]entity.Comment, error)
}
