package db

import (
	"context"
	"database/sql"
	"fmt"
	"testing"
	"time"
)

func TestTypeDataColumn(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	rows, err := db.QueryContext(ctx, "SELECT id, name, email, balance, rating, birth_date, created_at, married FROM customer")
	if err != nil {
		panic(err)
	}
	// ! try if we not use Close rows
	defer rows.Close()

	for rows.Next() {
		// *Nullable Type
		var id, name string
		var email sql.NullString
		var balance int32
		var rating float64
		var birthDate sql.NullTime
		var createdAt time.Time
		var married bool

		// *without nullable type
		//var id, name, email string
		//var balance int64
		//var rating float64
		//var createdAt, birthDate time.Time
		//var married bool

		err := rows.Scan(&id, &name, &email, &balance, &rating, &birthDate, &createdAt, &married)
		if err != nil {
			fmt.Println("err Scan : ", err)
			panic(err)
		}
		fmt.Println("Id : ", id, ", Name : ", name, "email : ", email, "balance : ", balance, "rating : ", rating, "createdAt : ", createdAt, "birthDate : ", birthDate, "married : ", married)
		// *bisa filter untuk mengatasi struct value yang berisi Nullable value
		if email.Valid {
			fmt.Println("Email : ", email.String)
		}
		if birthDate.Valid {
			fmt.Println("birthDate : ", birthDate.Time)
		}

	}

	//_, err := db.ExecContext(ctx, "INSERT INTO customer(id, name, email, balance, rating, birth_date, married) VALUES ('deri', 'Deri', 'deri@gmail.com', 950000, 9.9, '2008-07-18', false)")
	//if err != nil {
	//	panic(err)
	//}
	//fmt.Println("success insert data")

}
