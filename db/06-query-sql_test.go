package db

import (
	"context"
	"fmt"
	"testing"
)

func TestQuerySql(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	// *untuk select menggunakan QueryContext()
	rows, err := db.QueryContext(ctx, "SELECT * FROM customer")
	if err != nil {
		panic(err)
	}

	// *setelah menggunakan sql ini, wajib Close()
	defer rows.Close()

	// *looping data untuk membaca data
	fmt.Println(rows)
	for rows.Next() {
		var id, name string
		err := rows.Scan(&id, &name)
		if err != nil {
			panic(err)
		}

		fmt.Println("Id : ", id, ", Name : ", name)
	}
}
