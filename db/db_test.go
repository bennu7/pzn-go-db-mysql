package db

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"testing"
)

func TestOpenConnection(t *testing.T) {
	db, err := sql.Open("mysql", `root:Flypower167/\@tcp(localhost:3306)/pzn-db`)

	if err != nil {
		panic(err)
	}

	// *last gunakan close() ketika benar" sudah tidak butuh connection nya
	fmt.Println("done", err)
	defer db.Close()
}
