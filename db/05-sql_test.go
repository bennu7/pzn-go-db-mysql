package db

import (
	"context"
	"fmt"
	"testing"
)

func TestExecSql(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	script := "INSERT INTO customer(id, name) VALUES ('joko', 'Joko')"
	// !ExecContext() digunakan untuk mengexecute script sql, tanpa mengembalikan data, tidak bisa untuk perintah select
	_, err := db.ExecContext(ctx, script)
	if err != nil {
		panic(err)
	}
	fmt.Println("success insert data")
}

func TestExecSqlUpdate(t *testing.T) {
	db := GetConnection()
	defer db.Close()

	ctx := context.Background()
	script := "UPDATE customer SET name = 'Change Joko' WHERE id = 'joko'"
	_, err := db.ExecContext(ctx, script)
	if err != nil {
		panic(err)
	}
	fmt.Println("success update data")
}
