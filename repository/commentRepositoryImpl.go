package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/bennu7/pzn-go-db-mysql/entity"
	_ "github.com/go-sql-driver/mysql"
	"strconv"
)

// ?step ke 3 buat implementasi sesuai kontrak interface pada commentRepository
type commentRepositoryImpl struct {
	// *harus private commentRepositoryImpl, guna DB ini agar tidak perlu menambahkan lagi ke atribut param method
	DB *sql.DB
}

// ?last buat Implementasi New repository setelah menyelesaikan semua method pada kontrak interface
func NewCommentRepository(db *sql.DB) CommentRepository {
	// *gunakan pointer commentRepositoryImpl karena method" di atas menggunakan pointer juga saat pemanggilan DB pada commentRepositoryImpl
	return &commentRepositoryImpl{DB: db}
}

func (repo *commentRepositoryImpl) Insert(ctx context.Context, comment entity.Comment) (entity.Comment, error) {
	sqlExec := "INSERT INTO comment(email, comment) VALUES (?, ?)"
	result, err := repo.DB.ExecContext(ctx, sqlExec, comment.Email, comment.Comment)

	if err != nil {
		return comment, err
	}

	inserted, err := result.LastInsertId()
	if err != nil {
		return comment, err
	}

	comment.Id = int32(inserted)
	return comment, nil
}

func (repo *commentRepositoryImpl) FindById(ctx context.Context, id int32) (entity.Comment, error) {
	sqlQuery := "SELECT id,email,comment FROM comment WHERE id = ? LIMIT 1"
	rows, err := repo.DB.QueryContext(ctx, sqlQuery, id)
	comments := entity.Comment{}
	defer rows.Close()

	if err != nil {
		return comments, err
	}

	if rows.Next() {
		//ada data
		if err := rows.Scan(&comments.Id, &comments.Email, &comments.Comment); err != nil {
			return comments, err
		}

		return comments, nil
	} else {
		//tidak ada data
		fmt.Println("comments => ", comments)
		return comments, errors.New("id " + strconv.Itoa(int(id)) + " not found")
	}

	return comments, nil

}

func (repo *commentRepositoryImpl) FindAll(ctx context.Context) ([]entity.Comment, error) {
	sqlQuery := "SELECT id,email,comment FROM comment"
	rows, err := repo.DB.QueryContext(ctx, sqlQuery)
	var comments []entity.Comment

	if err != nil {
		return []entity.Comment{}, err
	}

	for rows.Next() {
		comment := entity.Comment{}

		if err := rows.Scan(&comment.Id, &comment.Email, &comment.Comment); err != nil {
			return comments, err
		}

		comments = append(comments, comment)
	}

	return comments, nil
}
