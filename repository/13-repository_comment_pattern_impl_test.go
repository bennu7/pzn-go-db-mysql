package repository

import (
	"context"
	"fmt"
	"github.com/bennu7/pzn-go-db-mysql/db"
	"github.com/bennu7/pzn-go-db-mysql/entity"
	"testing"
)

func TestCommentInsert(t *testing.T) {
	db := db.GetConnection()
	defer db.Close()
	ctx := context.Background()
	comment := entity.Comment{
		Email:   "second@gmail.com",
		Comment: "hahah saya raja barbar",
	}

	commentRepository := NewCommentRepository(db)
	result, err := commentRepository.Insert(ctx, comment)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}

func TestCommentDetail(t *testing.T) {
	db := db.GetConnection()
	defer db.Close()
	ctx := context.Background()
	comment := entity.Comment{
		Id: 20,
	}

	commentRepository := NewCommentRepository(db)
	result, err := commentRepository.FindById(ctx, comment.Id)
	if err != nil {
		panic(err)
	}

	fmt.Println("result ", result)
}

func TestGetAllComment(t *testing.T) {
	db := db.GetConnection()
	defer db.Close()
	ctx := context.Background()

	commentRepository := NewCommentRepository(db)
	results, err := commentRepository.FindAll(ctx)
	if err != nil {
		panic(err)
	}

	fmt.Println(results)
	for _, result := range results {
		fmt.Println(result)
	}

	//// *for loop dibawah ini untuk looping jumlah data
	//for result := range results {
	//	fmt.Println(result)
	//}

}
